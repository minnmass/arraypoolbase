﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;

namespace ArrayPoolBase {
	public class ArrayPoolQueue<T> : Queue<T> {
		private static readonly ArrayPoolConfig DefaultConfig = new ArrayPoolConfig {
			Shared = true,
			RentedArraySize = 128
		};

		private readonly ArrayPool<T> _pool;
		private readonly int _rentSize;
		private readonly LinkedList<T[]> _segments = new LinkedList<T[]>();

		private LinkedListNode<T[]> _writeSegment => _segments.Last;
		private int _writeHead = 0;

		private LinkedListNode<T[]> _readSegment => _segments.First;
		private int _readHead = 0;

		public ArrayPoolQueue(ArrayPoolConfig config = null) {
			if (config?.Shared ?? DefaultConfig.Shared) {
				_pool = ArrayPool<T>.Shared;
			} else {
				_pool = ArrayPool<T>.Create();
			}
			_rentSize = config?.RentedArraySize ?? DefaultConfig.RentedArraySize;

			AddSegment();
		}

		public ArrayPoolQueue(IEnumerable<T> collection, ArrayPoolConfig config = null) {
			if (config?.Shared ?? DefaultConfig.Shared) {
				_pool = ArrayPool<T>.Shared;
			} else {
				_pool = ArrayPool<T>.Create();
			}
			_rentSize = config?.RentedArraySize ?? DefaultConfig.RentedArraySize;

			AddSegment();

			foreach (var item in collection) {
				Enqueue(item);
			}
		}

		public ArrayPoolQueue(int capacity, ArrayPoolConfig config = null) {
			if (config?.Shared ?? DefaultConfig.Shared) {
				_pool = ArrayPool<T>.Shared;
			} else {
				_pool = ArrayPool<T>.Create();
			}
			_rentSize = config?.RentedArraySize ?? DefaultConfig.RentedArraySize;

			AddSegment();
		}

		public new int Count { get; private set; } = 0;

		public new void Clear() {
			_readHead = 0;
			foreach (var segment in _segments) {
				_pool.Return(segment);
			}
			AddSegment();
		}

		public new void Enqueue(T item) {
			if (_writeHead == _writeSegment.Value.Length) {
				AddSegment();
			}

			_writeSegment.Value[_writeHead] = item;
			++Count;
			++_writeHead;
		}

		public new bool TryPeek(out T result) {
			if (Count == 0) {
				result = default;
				return false;
			}

			result = _readSegment.Value[_readHead];
			return true;
		}

		public new T Peek() {
			return TryPeek(out T result)
				? result
				: throw new InvalidOperationException("Queue empty.");
		}

		public new bool TryDequeue(out T result) {
			if (Count == 0) {
				result = default;
				return false;
			}

			result = _readSegment.Value[_readHead];
			++_readHead;
			if (_readHead == _readSegment.Value.Length) {
				ReturnSegment();
			}
			--Count;

			return true;

			void ReturnSegment() {
				_pool.Return(_readSegment.Value, clearArray: true);
				_segments.RemoveFirst();
				_readHead = 0;
			}
		}

		public new T Dequeue() {
			return TryDequeue(out T result)
				? result
				: throw new InvalidOperationException("Queue empty.");
		}

		public new T[] ToArray() {
			return _segments
				.SelectMany(i => i)
				.Skip(_readHead)
				.Take(Count)
				.ToArray();
		}

		private void AddSegment() {
			var newSegment = _pool.Rent(_rentSize);
			_segments.AddLast(newSegment);
			_writeHead = 0;
		}
	}
}
