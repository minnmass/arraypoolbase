﻿namespace ArrayPoolBase {
	/// <summary>
	/// Configure the underlying ArrayPool. Null properties will use sane defaults.
	/// </summary>
	public class ArrayPoolConfig {
		/// <summary>
		/// Determines whether the underlying ArrayPool is shared; <see cref="ArrayPool{T}.Shared">ArrayPool.Shared</see>.
		/// </summary>
		public bool Shared { get; set; } = true;
		/// <summary>
		/// Determines the size of the <see cref="ArrayPool{T}.Rent(int)">rented array</see>.
		/// </summary>
		public int RentedArraySize { get; set; } = 128;
	}
}