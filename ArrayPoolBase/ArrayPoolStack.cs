﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;

namespace ArrayPoolBase {
	public class ArrayPoolStack<T> : Stack<T> {
		private static readonly ArrayPoolConfig DefaultConfig = new ArrayPoolConfig {
			Shared = true,
			RentedArraySize = 128
		};

		private readonly ArrayPool<T> _pool;
		private readonly int _rentSize;
		private readonly LinkedList<T[]> _segments = new LinkedList<T[]>();

		private LinkedListNode<T[]> _currentSegment = null;
		private int _writeHead = 0;

		public ArrayPoolStack(ArrayPoolConfig config = null) {
			if (config?.Shared ?? DefaultConfig.Shared) {
				_pool = ArrayPool<T>.Shared;
			} else {
				_pool = ArrayPool<T>.Create();
			}
			_rentSize = config?.RentedArraySize ?? DefaultConfig.RentedArraySize;
			AddSegment();
		}

		public ArrayPoolStack(IEnumerable<T> collection, ArrayPoolConfig config = null) {
			if (config?.Shared ?? DefaultConfig.Shared) {
				_pool = ArrayPool<T>.Shared;
			} else {
				_pool = ArrayPool<T>.Create();
			}
			_rentSize = config?.RentedArraySize ?? DefaultConfig.RentedArraySize;
			AddSegment();

			foreach (var element in collection) {
				Push(element);
			}
		}

		public ArrayPoolStack(int capacity, ArrayPoolConfig config = null) {
			if (config?.Shared ?? DefaultConfig.Shared) {
				_pool = ArrayPool<T>.Shared;
			} else {
				_pool = ArrayPool<T>.Create();
			}
			_rentSize = config?.RentedArraySize ?? DefaultConfig.RentedArraySize;
			AddSegment();
		}

		public new int Count { get; private set; } = 0;

		public bool IsSynchronized => false;

		public object SyncRoot = new object();

		public new void Clear() {
			Count = 0;
			_writeHead = 0;
			foreach (var segment in _segments) {
				_pool.Return(segment, clearArray: true);
			}
			_segments.Clear();
		}

		public new bool Contains(T item) {
			return _segments
				.SelectMany(i => i)
				.Take(Count)
				.Any(i => ReferenceEquals(i, item) || i is object && i.Equals(item));
		}

		public new System.Collections.IEnumerator GetEnumerator() {
			return _segments
				.SelectMany(i => i)
				.Take(Count)
				.GetEnumerator();
		}

		public new void Push(T item) {
			if (_writeHead == _currentSegment.Value.Length) {
				AddSegment();
			}

			_currentSegment.Value[_writeHead] = item;
			++_writeHead;
			++Count;
		}

		public new T[] ToArray() {
			return _segments
				.SelectMany(i => i)
				.Take(Count)
				.Reverse()
				.ToArray();
		}

		public new T Peek() {
			return TryPeek(out T result)
				? result
				: throw new InvalidOperationException("Stack empty.");
		}

		public new bool TryPeek(out T result) {
			if (Count == 0) {
				result = default;
				return false;
			}

			if (_writeHead > 0) {
				result = _currentSegment.Value[_writeHead - 1];
				return true;
			}
			var previousSegment = _currentSegment.Previous;
			if (previousSegment is null) {
				result = default;
				return false;

			}
			result = previousSegment.Value[^1];
			return true;
		}

		public new T Pop() {
			return TryPop(out T result)
				? result
				: throw new InvalidOperationException("Stack empty.");
		}

		public new bool TryPop(out T result) {
			if (Count == 0) {
				result = default;
				return false;
			}

			--Count;
			--_writeHead;

			if (_writeHead < 0) {
				ReturnCurrentSegment();
			}
			result = _currentSegment.Value[_writeHead];

			return true;

			void ReturnCurrentSegment() {
				if (_currentSegment.Previous is null) {
					_writeHead = 0;
					return;
				}
				_pool.Return(_currentSegment.Value, clearArray: true);
				_currentSegment = _currentSegment.Previous;
				_writeHead = _currentSegment.Value.Length - 1;
			}
		}

		private void AddSegment() {
			var newSegment = _pool.Rent(_rentSize);
			_segments.AddLast(newSegment);
			_currentSegment = _currentSegment is null
				? _segments.First
				: _currentSegment.Next;
			_writeHead = 0;
		}
	}
}
