﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ArrayPoolBase.Tests {
	public class ArrayPoolQueueTests {
		private static readonly IReadOnlyCollection<int> _simpleCollection = new[] { 1, 2, 3, 4, 5 };

		[Fact]
		public void Constructor_Collection_EnqueuesCollection() {
			var sut = new ArrayPoolQueue<int>(_simpleCollection);

			var asArray = sut.ToArray();

			Assert.Equal(_simpleCollection, asArray);
		}

		[Fact]
		public void Enqueue_DequeueDequeues() {
			var sut = new ArrayPoolQueue<int>();
			const int expected = 1;
			sut.Enqueue(expected);

			var actual = sut.Dequeue();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Enqueue_Multiple_AddsToArrayInOrder() {
			var sut = new ArrayPoolQueue<int>();

			for (int i = 0; i < _simpleCollection.Count; ++i) {
				sut.Enqueue(_simpleCollection.ElementAt(i));

				Assert.Equal(_simpleCollection.Take(i + 1).ToArray(), sut.ToArray());
			}
		}

		[Fact]
		public void Dequeue_Empty_Throws() {
			var sut = new ArrayPoolQueue<int>();

			var exception = Record.Exception(() => sut.Dequeue());

			Assert.IsType<InvalidOperationException>(exception);
			Assert.Equal("Queue empty.", exception.Message);
		}

		[Fact]
		public void TryDequeue_Empty_ReturnsFalse() {
			var sut = new ArrayPoolQueue<int>();

			var actual = sut.TryDequeue(out _);

			Assert.False(actual);
		}

		[Fact]
		public void Dequeue_NonEmpty_ReturnsInCorrectOrder() {
			var sut = new ArrayPoolQueue<int>();
			foreach (var element in _simpleCollection) {
				sut.Enqueue(element);
			}

			foreach (var expected in _simpleCollection) {
				var actual = sut.Dequeue();

				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		public void TryDequeue_NonEmpty_ReturnsInCorrectOrder() {
			var sut = new ArrayPoolQueue<int>();
			foreach (var element in _simpleCollection) {
				sut.Enqueue(element);
			}

			foreach (var expected in _simpleCollection) {
				_ = sut.TryDequeue(out int actual);

				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		public void Enqueue_ManyObjects_EnquesAllInCorrectOrder() {
			var sut = new ArrayPoolQueue<int>();
			var elements = Enumerable.Range(1, 100_000).ToArray();
			foreach (var element in elements) {
				sut.Enqueue(element);
			}

			foreach (var expected in elements) {
				var actual = sut.Dequeue();
				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		public void Count_NewQueue_IsZero() {
			var sut = new ArrayPoolQueue<int>();

			var actual = sut.Count;

			Assert.Equal(0, actual);
		}

		[Fact]
		public void Count_Counts() {
			var sut = new ArrayPoolQueue<int>();
			Test(0);

			sut.Enqueue(1);
			Test(1);

			_ = sut.Dequeue();
			Test(0);

			foreach (var i in _simpleCollection) {
				sut.Enqueue(i);
			}
			Test(_simpleCollection.Count);

			_ = sut.Dequeue();
			_ = sut.Dequeue();
			Test(_simpleCollection.Count - 2);

			for (int i = 0; i < 4; ++i) {
				sut.Enqueue(i);
			}
			Test(_simpleCollection.Count - 2 + 4);

			void Test(int expected) {
				var actual = sut.Count;
				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		public void Peek_EmptyQueue_Throws() {
			var sut = new ArrayPoolQueue<int>();

			var exception = Record.Exception(() => sut.Peek());

			Assert.IsType<InvalidOperationException>(exception);
			Assert.Equal("Queue empty.", exception.Message);
		}

		[Fact]
		public void Peek_NonEmptyQueue_Peeks() {
			var sut = new ArrayPoolQueue<int>(_simpleCollection);
			var expected = _simpleCollection.First();

			for (int i = 0; i < 4; ++i) {
				var actual = sut.Peek();

				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		public void GeneralVerification() {
			var realQueue = new Queue<int>();
			var sut = new ArrayPoolQueue<int>();

			var elements = Enumerable.Range(1, 10_000).ToArray();

			foreach (var element in elements) {
				realQueue.Enqueue(element);
				sut.Enqueue(element);

				Assert.Equal(realQueue.ToArray(), sut.ToArray());
				Assert.Equal(realQueue.Peek(), sut.Peek());
			}

			while (realQueue.Count > 0) {
				var expected = realQueue.Dequeue();
				var actual = sut.Dequeue();

				Assert.Equal(expected, actual);
			}
		}
	}
}
