﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ArrayPoolBase.Tests {
	public class ArrayPoolStackTests {
		private static readonly IReadOnlyCollection<int> _simpleCollection = new[] { 1, 2, 3, 4, 5 };

		[Fact]
		public void Constructor_Collection_EnqueuesCollection() {
			var sut = new ArrayPoolStack<int>(_simpleCollection);

			var asArray = sut.ToArray();

			Assert.Equal(_simpleCollection.Reverse(), asArray);
		}

		[Fact]
		public void ToArray_EmptyStack_ReturnsEmptyArray() {
			var sut = new ArrayPoolStack<int>();

			var actual = sut.ToArray();

			Assert.Empty(actual);
		}

		[Fact]
		public void Enqueue_PopPops() {
			var sut = new ArrayPoolStack<int>();
			const int expected = 1;
			sut.Push(expected);

			var actual = sut.Pop();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Enqueue_Multiple_AddsToArrayInOrder() {
			var sut = new ArrayPoolStack<int>();

			for (int i = 0; i < _simpleCollection.Count; ++i) {
				sut.Push(_simpleCollection.ElementAt(i));

				Assert.Equal(_simpleCollection.Take(i + 1).Reverse().ToArray(), sut.ToArray());
			}
		}

		[Fact]
		public void Pop_Empty_Throws() {
			var sut = new ArrayPoolStack<int>();

			var exception = Record.Exception(() => sut.Pop());

			Assert.IsType<InvalidOperationException>(exception);
			Assert.Equal("Stack empty.", exception.Message);
		}

		[Fact]
		public void TryPop_Empty_ReturnsFalse() {
			var sut = new ArrayPoolStack<int>();

			var actual = sut.TryPop(out _);

			Assert.False(actual);
		}

		[Fact]
		public void Pop_NonEmpty_ReturnsInCorrectOrder() {
			var sut = new ArrayPoolStack<int>();
			foreach (var element in _simpleCollection) {
				sut.Push(element);
			}

			foreach (var expected in _simpleCollection.Reverse()) {
				var actual = sut.Pop();

				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		public void TryPop_NonEmpty_ReturnsInCorrectOrder() {
			var sut = new ArrayPoolStack<int>();
			foreach (var element in _simpleCollection) {
				sut.Push(element);
			}

			foreach (var expected in _simpleCollection.Reverse()) {
				_ = sut.TryPop(out int actual);

				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		public void Enqueue_ManyObjects_EnquesAllInCorrectOrder() {
			var sut = new ArrayPoolStack<int>();
			var elements = Enumerable.Range(1, 1_000_000).ToArray();
			foreach (var element in elements) {
				sut.Push(element);
			}

			foreach (var expected in elements.Reverse()) {
				var actual = sut.Pop();
				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		public void Count_NewQueue_IsZero() {
			var sut = new ArrayPoolStack<int>();

			var actual = sut.Count;

			Assert.Equal(0, actual);
		}

		[Fact]
		public void Count_Counts() {
			var sut = new ArrayPoolStack<int>();
			Test(0);

			sut.Push(1);
			Test(1);

			_ = sut.Pop();
			Test(0);

			foreach (var i in _simpleCollection) {
				sut.Push(i);
			}
			Test(_simpleCollection.Count);

			_ = sut.Pop();
			_ = sut.Pop();
			Test(_simpleCollection.Count - 2);

			for (int i = 0; i < 4; ++i) {
				sut.Push(i);
			}
			Test(_simpleCollection.Count - 2 + 4);

			void Test(int expected) {
				var actual = sut.Count;
				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		public void Peek_EmptyQueue_Throws() {
			var sut = new ArrayPoolStack<int>();

			var exception = Record.Exception(() => sut.Peek());

			Assert.IsType<InvalidOperationException>(exception);
			Assert.Equal("Stack empty.", exception.Message);
		}

		[Fact]
		public void Peek_NonEmptyQueue_Peeks() {
			var sut = new ArrayPoolStack<int>(_simpleCollection);
			var expected = _simpleCollection.Last();

			for (int i = 0; i < 4; ++i) {
				var actual = sut.Peek();

				Assert.Equal(expected, actual);
			}
		}

		[Fact]
		public void Push_Drain_Push_Works() {
			var sut = new ArrayPoolStack<int>();

			foreach (var i in _simpleCollection) {
				sut.Push(i);
			}

			for(int i = 0; i < _simpleCollection.Count; ++i) {
				_ = sut.Pop();
			}

			// quick reality check
			Assert.Equal(0, sut.Count);

			foreach (var i in _simpleCollection) {
				sut.Push(i);
			}

			Assert.Equal(_simpleCollection.Reverse(), sut.ToArray());
		}

		[Fact]
		public void GeneralVerification() {
			var realStack = new Stack<int>();
			var sut = new ArrayPoolStack<int>();

			var elements = Enumerable.Range(1, 10_000).ToArray();

			foreach (var element in elements) {
				realStack.Push(element);
				sut.Push(element);

				Assert.Equal(realStack.ToArray(), sut.ToArray());
				Assert.Equal(realStack.Peek(), sut.Peek());
			}

			while (realStack.Count > 0) {
				var expected = realStack.Pop();
				var actual = sut.Pop();

				Assert.Equal(expected, actual);
			}
		}

	}
}
